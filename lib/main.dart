import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:task_list/task-create.dart';
import 'package:task_list/task-list.dart';
import 'package:task_list/user-register.dart';

const firebaseConfig = FirebaseOptions(
    apiKey: "AIzaSyAZTkBralhI4HyYAq4T25DA9WVxF8ywDeY",
    authDomain: "task-list-aa6fc.firebaseapp.com",
    projectId: "task-list-aa6fc",
    storageBucket: "task-list-aa6fc.appspot.com",
    messagingSenderId: "574267901379",
    appId: "1:574267901379:web:1a57d2602ad0294fa66976");

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(options: firebaseConfig);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // home: TaskListPage(),
      initialRoute: '/user-register',
      routes: {
        '/task-create': (context) => TaskCreatePage(),
        '/task-list': (context) => TaskListPage(),
        '/user-register': (context) => UserRegisterPage()
      },
    );
  }
}
